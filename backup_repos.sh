#!/usr/bin/env bash
##
## backup_repos.sh
##
## Loops over each GitLab repo name in <INPUT_FILE> and clones them to
## <DEST_DIR>.
##
# Check argument count
if [ "$#" -ne 2 ]; then
  echo "Usage: clone_repos.sh <INPUT_FILE> <DEST_DIR>"
  exit 1
fi
INPUT_FILE="$1"
DEST_DIR="$2"

# Validate arguments
if [ ! -f "$INPUT_FILE" ]; then
  echo "Error: input file does not exist"
  exit 1
fi
if [ ! -w "$DEST_DIR" ]; then
  echo "Error: destination must exist and be writable"
  exit 1
fi

# Creates a sub directory in <DEST_DIR> to contain the repos. Put the
# current date in the name of the sub-dir.
SUB_DIR="gitlab_repos_`date +%Y-%m-%d`"
# If sub-dir already exists (due to prior run), delete it
if [ -d "$DEST_DIR/$SUB_DIR" ]; then
  rm -rf "$DEST_DIR/$SUB_DIR"*
fi
mkdir -p "$DEST_DIR/$SUB_DIR"

# Loop over repo names
while read repo; do
  echo "Cloning $repo"
  git clone git@gitlab.com:ian-s-mcb/"$repo" "$DEST_DIR/$SUB_DIR/$repo"
done < "$INPUT_FILE"
echo "Cloning complete"

# Create tar ball
cd "$DEST_DIR"
echo "Compressing repos"
tar czf "$SUB_DIR".tar.gz "$SUB_DIR"
echo "Compression complete"
