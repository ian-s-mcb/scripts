#!/usr/bin/env bash
#
# color-theme
#
# Retrieves or toggles the color theme for the terminal emulator and Gnome apps.
#
# Requires that
#   - the pacman package `gnome-themes-extra` to be installed
#   - two alacritty theme files placed in `$HOME/alacritty/themes`
#   - a text file named `color.state` containing the current theme (light or
#     dark) placed in the same directory as this script
#   - a line in the alacritty config file that imports the appropriate
#     alacritty theme file
#

print_usage()
{
cat << EOF
Usage: color-theme <option>

Options:
  -h, --help,   Display this help message
  -g, --get,    Get color theme
  -t, --toggle, Toggle color theme
EOF
}

toggle_alacritty()
{
  declare -A themes=(
    [light]="atom_one_light"
    [dark]="atom_one_dark"
  )

  sed -i "s/${themes[$current_color]}/${themes[$toggled_color]}/" "$CONFIG_ALACRITTY"
}

toggle_gtk()
{
  declare -A themes=(
    [light]="Adwaita"
    [dark]="Adwaita-dark"
  )

  # Apply change to gtk
  sed -i 's/Adwaita-dark\|Adwaita/'"${themes[$toggled_color]}"'/' "$CONFIG_GTK"
  import-gsettings
}

# Constants
STATE_FILE="${HOME}/dev/dev-current/scripts/color.state"
CONFIG_ALACRITTY="${XDG_CONFIG_HOME:-$HOME/.config}/alacritty/alacritty.toml"
CONFIG_GTK="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-3.0/settings.ini"

# Sanity checks
test ! -e "$STATE_FILE" && echo 'Error: color.state file not found' && exit 1
if [[ "$#" -ne 1 ]]; then
  echo "No option provided"
  print_usage
  exit 1;
fi

current_color=$(< "$STATE_FILE")
toggled_color=$([ "$current_color" == "dark" ] && echo "light" || echo "dark")

# Handle options
case "$1" in
  (-h|--help)
    print_usage
    exit 0
  ;;
  (-g|--get)
    echo "$current_color"
    exit 0
  ;;
  (-t|--toggle)
    toggle_alacritty
    toggle_gtk
    echo "$toggled_color" > "$STATE_FILE"
    exit 0
  ;;
  (*)
    echo "Incorrect option"
    print_usage
    exit 1
esac
